#!/bin/sh
#
#    Copyright (C) 2024 Luis Guzmán <ark@switnet.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=0
. ./config

export FULLVERSION="$(sed 's|1.3.0-4|1.3.0-4+really1.3.0-5|' <<< $FULLVERSION)"

# Upgrade 1.3.0-4 to 1.3.0-5
patch_p1 $DATA/guix-1.3.0.4-to-1.3.0-5.patch

# Apply missing CVEs upstream:
## 1.CVE-2024-27297
for patch in $(ls -v ${DATA}/cve/*.patch)
do
    patch_p1 $patch
done

# TODO: Confirm fix for nix package.
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1066812

changelog "Upgrade to 1.3.0-5 and apply missing CVE-2024-27297 upstream patch."

package
